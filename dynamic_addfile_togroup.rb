require 'xcodeproj'
project_path = "../RubyTestDemo/RubyTestDemo.xcodeproj"
project = Xcodeproj::Project.open(project_path)
project.targets.each do |target|
  puts target.name
end
target = project.targets.first

target.build_configurations.each do |config|
  if config.name == "Debug"
    config.build_settings.each do |key, value|
      puts key,value
    end
  end
end

#find_subpath在主工程顶点开始找，找到对应的文件夹然后在其中创建分组
group = project.main_group.find_subpath(File.join("RubyTestDemo/Model"), true )
group.set_source_tree("SOURCE_ROOT")#创建一个正常的文件夹（设置文件夹的引用类型）

file_ref_h = group.new_reference("test.h")
file_ref_m = group.new_reference("test.m")#注意这里的test.h 和test.m文件是必须存在的

file_ref_h.set_source_tree("SOURCE_ROOT")

file_ref_h.set_path("Code/test.h")

file_ref_h.name = "test.h"

file_ref_m.set_source_tree("SOURCE_ROOT")

file_ref_m.set_path("Code/test.m")

file_ref_m.name = "test.m"
target.add_file_references([file_ref_m])#向group中增加文件引用（.h文件只需引用一下，.m引用后还需add一下）
project.save