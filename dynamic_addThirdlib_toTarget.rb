require 'xcodeproj'
project_path = "../RubyTestDemo/RubyTestDemo.xcodeproj"

project = Xcodeproj::Project.open(project_path)

target = project.targets.first

target.build_configurations.each do |config|
  if config.name == "Debug"
    config.build_settings.each do |key, value|
      puts "#{key}"
    end
  end
end

group = project.main_group.find_subpath("RubyTestDemo/NewGroup",true)
file_path = "libzbar.a"
file_ref = group.new_reference(file_path)

file_ref.set_source_tree("SOURCE_ROOT")
file_ref.name = "libzbar.a"
file_ref.set_path("ThirdLib/libzbar.a")
#和添加普通文件多了一步这个 需要编译的步骤
target.frameworks_build_phases.add_file_reference(file_ref)
target.build_configurations.each do |config|
  #必须写这句话不写 找不到路径
  config.build_settings["LIBRARY_SEARCH_PATHS"] = ["$(inherited)","$(PROJECT_DIR)/ThirdLib"]
end
project.save