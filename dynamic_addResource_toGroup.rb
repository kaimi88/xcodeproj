require 'xcodeproj'
project_path = "../RubyTestDemo/RubyTestDemo.xcodeproj"

project = Xcodeproj::Project.open(project_path)

target = project.targets.first

group = project.main_group.find_subpath("RubyTestDemo/NewGroup",true)

target.build_configurations.each do |config|
  puts config.name

  if config.name == "Debug"
    config.build_settings.each do |key, value|
      puts key
      puts value
    end
  end
end

file_path = "testXcode.bundle"
file_ref = group.new_reference(file_path)
file_ref.set_source_tree("SOURCE_ROOT")
file_ref.set_path('Resource/testXcode.bundle')
file_ref.name = "testXcode.bundle"
#加到文件组内
target.add_resources([file_ref])
project.save