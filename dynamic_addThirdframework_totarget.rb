require 'xcodeproj'
project_path = "../RubyTestDemo/RubyTestDemo.xcodeproj"
project = Xcodeproj::Project.open(project_path)
file_path = "ThirdFramework/BMKLocationKit.framework"
file_ref = project.frameworks_group.new_reference(file_path)
file_ref.set_source_tree("SOURCE_ROOT")
file_ref.name = "BMKLocationKit.framework"
target = project.targets.first
project.targets.each do |target, index|
  puts "#{index}:#{target}"
end

target.frameworks_build_phases.add_file_reference(file_ref)
target.build_configurations.each do |config|
  #这句话必须写 不写的话找不到路径
  config.build_settings["FRAMEWORK_SEARCH_PATHS"] = ["$(inherited)","$(PROJECT_DIR)/ThirdFramework"]
end
project.save

