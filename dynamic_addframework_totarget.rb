require 'xcodeproj'
project_path = "../RubyTestDemo/RubyTestDemo.xcodeproj"

project = Xcodeproj::Project.open(project_path)

target = project.targets.first

target.build_configurations.each do |config|
  puts config.name
  config.build_settings.each do |key, value|
    puts key
    puts value
  end
end

#创建framework文件引用
file_path = "System/Library/Frameworks/QuartzCore.framework"
file_ref = project.frameworks_group.new_reference(file_path)
file_ref.set_source_tree("SDKROOT")
file_ref.name = "QuartzCore.framework"

#添加文件的引用
target.frameworks_build_phases.add_file_reference(file_ref)

project.save





